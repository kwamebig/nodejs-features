const { Router } = require('express');
const AuthService = require('../services/authService');

const router = Router();

router.post('/login', (req, res, next) => {

    try {
        const checkEmail = AuthService.login({ email: req.headers.authorization });

        if(!checkEmail) {
            return res.status(404).json({ error: 404, message: "User not found" });
        } else if(req.body.password !== checkEmail.password) {
            return res.status(403).json({ error: 403, message: "Wrong password. Please try again" });
        } else {
            return res.status(200).json({ message: "Authorization granted" });
        }
    } catch(err) {
        next(err);
    }
});

module.exports = router;