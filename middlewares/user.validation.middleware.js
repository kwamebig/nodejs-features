const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {

    try {
        const temp = {}; 
        const emailValidation = /^[a-zA-Z0-9.$!#%&\'*+\/=?^_`{|}~-]+@gmail.com$/;
        const phoneNumberValidation = /^\+380\d{9}$/;

        if(req.method === 'POST') {
            Object.keys(user).filter(elem => elem !== 'id').forEach(elem => {
                if(!req.body.hasOwnProperty(elem)) {
                    temp[elem] = `${elem} is required`;
                }
            });
        }
        
        Object.keys(req.body).forEach(elem => {
            if(elem === 'id' ||
            elem === 'createdAt' ||
            elem === 'updatedAt') {
                temp[elem] = 'This property is not available';
                return;
            } else if(!user.hasOwnProperty(elem)) {
                temp[elem] = `${elem} is not defined for user`;
                return;
            }

            if(req.body[elem] === '') {
                temp[elem] = `${elem} is empty`;
            } else if(req.body[elem].length > 64) {
                temp[elem] = `${elem} surprass the characters limit`;
            }
        });


        if(req.body.email && !temp.email) {
            if(UserService.search({ email: req.body.email })) {
                temp.email = 'This e-mail already exists';
            } else if(!req.body.email.match(emailValidation)) {
                temp.email = 'Invalid e-mail';
            }
        }
        

        if(req.body.phoneNumber && !temp.phoneNumber) {
            if(UserService.search({ phoneNumber: req.body.phoneNumber })) {
                temp.phoneNumber = 'Phone number already exists';
            } else if(!req.body.phoneNumber.match(phoneNumberValidation)) {
                temp.phoneNumber = 'Invalid phone number';
            }
        }

        if(req.body.password &&
            !temp.password && 
            req.body.password.length < 3) {
                temp.password = 'Password is too short';
        }

        if(!(Object.entries(temp).length === 0)) {
            return res.status(400).json({ 
                error: 400, 
                message: temp 
            });
        } else {
            next();
        }
    } catch(err) {
        next(err);
    }
}


exports.createUserValid = createUserValid;