const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {

    try {
        const temp = {}; 

        if(req.method === 'POST') {
            Object.keys(fighter).filter(elem => elem !== 'id').forEach(elem => {
                if(!req.body.hasOwnProperty(elem)) {
                    temp[elem] = `${elem} is required`;
                }
            });
        }
        

        Object.keys(req.body).forEach(elem => {
            if(elem === 'id' ||
            elem === 'createdAt' ||
            elem === 'updatedAt') {
                temp[elem] = 'This property is not available';
                return void 0;
            } else if(!fighter.hasOwnProperty(elem)) {
                temp[elem] = `${elem} is not defined for fighter`;
                return void 0;
            }

            if(req.body[elem] === '') {
                temp[elem] = `${elem} is empty`;
            } else if(req.body[elem].length > 64) {
                temp[elem] = `${elem} surpass the characters limit`;
            } else if(elem !== 'name' && typeof elem !== 'number') {
                temp[elem] = `${elem} must be a number`;
            }
        });

        if(req.body.health &&
            !temp.health &&
            (req.body.health > 100 ||
            req.body.health < 20)) {
                temp.health = "Health value must be in range from 20 to 100";
        }

        if(req.body.power &&
            !temp.power &&
            (req.body.power > 10 ||
            req.body.power < 1)) {
                temp.power = "Power value must be in range from 1 to 10";
        }

        if(req.body.defense &&
            !temp.defense &&
            (req.body.defense > 10 ||
            req.body.defense < 1)) {
                temp.defense = "Power value must be in range from 1 to 10";
        }

        if(!(Object.entries(temp).length === 0)) {
            return res.status(400).json({ error: 400, message: temp });
        } else {
            next();
        }
    } catch(err) {
        next(err);
    }
}

const updateFighterValid = (req, res, next) => {
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;