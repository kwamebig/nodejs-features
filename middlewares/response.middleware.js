const responseMiddleware = (req, res, next) => {
  
   if(res.status(200)) {
    return res.status(200).json({
        error: false,
        message: 'OK'
    });
} else if(res.status(400)) {
    return res.status(400).json({ 
        error: true, 
        message: 'Bad request' 
    })
} 

else if(res.status(500)) {
    return res.status(400).json({ 
        error: true, 
        message: 'Internal server error' 
    })
}

else {
    next();

}

}

exports.responseMiddleware = responseMiddleware;